<?php

//ver los contenidos de un directorio
$contenidos = scandir("public");



// //quitar el dir actual . y el nivel superior ..
$contenidos = array_diff($contenidos, ['.', '..']);
var_dump($contenidos);

// // //contenidos con salto de linea
// foreach ($contenidos as $elemento) {
//     echo "<br>$elemento<br>";
// }
echo '<hr>';
// //contenidos con salto de linea, 
// //y verificar cual es archivo y cual carpeta
foreach ($contenidos as $elemento) {
    //se tiene que incluir el path
    if (is_dir(('public/' . $elemento)))
        echo "<br>Es carpeta: $elemento<br>";
    else if (is_file('public/' . $elemento))
        echo "<br>Es archivo: $elemento<br>";
}

// //crear carpeta -----------------------
// mkdir("public/webclientes");
// $contenidos = scandir("public");
// print_r($contenidos);

// //evitar error
// if (file_exists('public/webclientes') && is_dir('public/webclientes'))
//     echo 'Ya existe public/webclientes';
// else {
//     mkdir("public/webclientes");
//     $contenidos = scandir("public");
//     print_r($contenidos);
// }

// // //borrar carpeta
// rmdir('public/webclientes');
// if (file_exists('public/webclientes') && is_dir('public/webclientes'))
//     echo '<br>Ya existe public/webclientes<br>';
// else
//     echo '<br>No existe o se BORRO: public/webclientes<br>';


// //-----------------------------------
// //desplegar contenidos por tipo
// $imgs = glob("public/assets/*.png");
// var_dump($imgs);

mkdir('public/imgs');

// //copiar archivos---------------
copy("public/assets/ameciderLogo.jpg", "public/imgs/amedicer_logo.jpg");
$imgs = scandir('public/assets');
// var_dump($imgs);
foreach ($imgs as $img) {
    if (!is_dir('public/assets/' . $img))
        copy("public/assets/" . $img, "public/imgs/" . $img);
}

// //cambiar nombre archs
// rename('public/imgs/amedicer_logo.jpg', 'public/imgs/ame__logo.jpg');

// // //borrar archivos
// unlink('public/imgs/ame__logo.jpg');


// //--------------------------------
// //Contenidos de archivos

// //abrir archivo en 3 modos: r/w/a -> read/write/append

// //con metodo file_get_contents
// $contenido = file_get_contents('public/imgs/cemex-logo-vector.png');
// echo "<hr><br>$contenido";

// //--------fopen-----fclose---------
// //lectura
// $arch = fopen('public/imgs/cemex-logo-png-transparent.png', 'r');
// $tamaño = filesize('public/imgs/cemex-logo-png-transparent.png');
// $contenido = fread($arch, $tamaño);
// echo "<hr><br>$contenido";
// fclose($arch);

// //escritura
// $arch = fopen('public/imgs/cemex-logo-vector.png', 'w');
// // $tamaño = filesize('public/imgs/cemex-logo-vector.png');
// $contenido = fwrite($arch, 'dato incorrecto en imagen');
// fclose($arch);

//file_put_contents("prueba.txt", " Nada de pruebas... etc.");

// //agregar----
// $arch = fopen('prueba.txt', 'a');
// $tamaño = filesize('prueba.txt');
// $contenido = fwrite($arch, '->>>> TEXTO AÑADIDO otras pruebas');
// fclose($arch);

// /////////////////////////////////
// //arch config

$config = parse_ini_file('configbd.ini');
foreach ($config as $key => $value) {
    echo "<br>$key => $value";
}

$connectionString = "mysql:" . $config['host'] . ":" . $config['port'] . " us=" . $config['us'] . ";pwd=" . $config['pwd'] . ";dbname=" . $config['dbname'];
echo "<br>con string=" . $connectionString;