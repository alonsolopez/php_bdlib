<?php

class CriterioDeBusqueda
{
    //SELECT * from clientes WHERE 
    // apellido_paterno = 'LOPEZ' OR limite_credito = 10000.00 
    public function __construct($campo, $op, $valor, $tipoValor, $opLogico)
    {
        $this->campo = $campo;
        $this->op = $op;
        $this->valor = $valor;
        $this->tipoValor = $tipoValor;
        $this->opLogico = $opLogico;
    }
    const OP_IGUAL = ' = ';
    const OP_DIFERENTE = ' <> '; //! =  |||   <>
    const OP_MENOR_IGUAL = ' <= ';
    const OP_MAYOR_IGUAL = ' >= ';
    const OP_LIKE = ' LIKE ';
    //  const OP_LIKE = ' LIKE ';
    const OP_LOGICO_AND = ' AND ';
    const OP_LOGICO_OR = ' OR ';
    const OP_LOGICO_NONE = '  ';


    private $campo;
    private $op;
    private $valor;
    private $tipoValor; //con '' o sin ''
    private $opLogico;

    public function get()
    {
        $valor = $this->tipoValor == true ? "'$this->valor'" : "$this->valor";
        //     "   nombre  =  'MORENO' OR  "
        return " $this->campo $this->op $valor $this->opLogico";
    }
}
