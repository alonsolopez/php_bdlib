<?php
//session PHP
session_start();
if (isset($_SESSION['loginCliente'])) {
}

//enlazar el archivo (include o REQUIRE)
require_once "modulos/cajas/Caja.php";
//crear instancia
$caja = new Caja('', '', '');
//validar el envio de valores
if (isset($_POST['btnGuardar'])) {
    $nom = $_POST['txtNom'];
    $mac = $_POST['txtMac'];
    $descrip = $_POST['txtDescrip'];

    //hacer insert --------------------------------------
    $res = $caja->insertar($nom, $mac, $descrip);
    if ($res === true) {
        $alert = "<script>alert('Caja Registrada correctamente...');</script>";
    } else {
        $alert = "<script>alert('ERROR CAJA NO Registrada ...');</script>";
    }
    print $alert;

    if ($caja->eliminar(60))
        print "Se borro el 60";

    if ($caja->modificar('jA999', '898789FA999', 'Caja 999= de pruebas.....', 49))
        print "Se actializo el 49";
}
$caja->consultarTodos();
echo '<br><hr><br>';

//arreglo para consultar!!!
$arrayCriterios = [
    "id" => 49,
    "nombre" => 'jA999',
];

// $whereDescrito = " nombre='CAJA#2' AND (mac_address = '080027d6f576' OR mac_address = '080027d6f000')";

//SELECT * FROM cajas WHERE id=49 AND nombre = 'jA999'
print_r($caja->consultaEspecifica($arrayCriterios));

$arrayCriteriosBusqueda = [
    "0" => new CriterioDeBusqueda('nombre', CriterioDeBusqueda::OP_IGUAL, 'CAJA#4', true, CriterioDeBusqueda::OP_LOGICO_OR),
    "1" => new CriterioDeBusqueda('mac_address', CriterioDeBusqueda::OP_LIKE, '%8%', true, CriterioDeBusqueda::OP_LOGICO_NONE),

];
print_r($caja->consultaEspecificaArray($arrayCriteriosBusqueda));
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1>Portal Admin</h1>
    <nav>
        <ul>
            <li>
                <a href="Dashboard">Dashboard</a>
                <a href="Productos">Catalogo de Productos</a>
                <a href="Almacen">Almacen</a>
                <a href="Usuarios">Usuarios</a>
                <a href="Proveedores">Proveedor</a>
            </li>
        </ul>
    </nav>
    <hr>
    <h2>REGISTRO DE CAJA</h2>
    <form action="CajasController.php" method="POST">
        Nombre<input type="text" name="txtNom" id="txtNom"><br>
        MAC:<input type="text" name="mac_address" id="txtMac"><br>
        Descripción<textarea name="txtDescrip" id="txtDescrip"></textarea><br>
        <hr>
        <button type="submit" value="Enviar" name="btnGuardar">GUARDAR</button>
    </form>
</body>

</html>